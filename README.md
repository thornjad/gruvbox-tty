# Gruvbox themes for TTY

These themes are based on [Gruvbox for Vim](https://github.com/morhetz/gruvbox) by [@morhetz](https://github.com/morhetz). They can be used to color the TTY terminals in Linux, and possibly others.

To use, copy the text from either the [light](gruvbox-light.txt) or [dark](gruvbox-dark.txt) theme file and put it anywhere in your `~/.bashrc` or
`~/.zshrc`, and enjoy!

[MIT license](LICENSE)
